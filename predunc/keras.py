import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import backend as K
import matplotlib.pyplot as plt
from tensorflow.python.keras import Input
from tensorflow.python.keras.layers import Dense, Dropout
from tensorflow.python.keras.models import Model

from predunc.settings import P_DROPOUT


def build_model(n_in, n_out, mc_dropout, p_dropout=P_DROPOUT, n_dense=20):
    """
    Build regression model.

    Args:
        n_in (tuple): input shape.
        n_out (int): number of output neurons.
        mc_dropout (bool): whether to use Monte Carlo dropout layers (True) or normal ones (False).
        p_dropout (float): drop probability for dropout layers.
        n_dense (int): optional number of neurons in dense layers. Defaults to np.prod(n_in).

    Returns:
        model (keras.Model): model.
    """
    if mc_dropout:
        # Monte Carlo dropout (also enabled during inference/testing).
        dropout = MCDropout
    else:
        # Normal dropout (only enabled during training).
        dropout = Dropout

    # Number of Dense units.
    if n_dense is None:
        n_dense = np.prod(n_in)

    # Build model.
    inputs = Input(n_in)
    x = Dense(n_dense, activation='relu')(inputs)
    x = dropout(p_dropout)(x)
    x = Dense(n_dense, activation='relu')(x)
    x = dropout(p_dropout)(x)
    x = Dense(n_dense, activation='relu')(x)
    x = dropout(p_dropout)(x)
    outputs = Dense(n_out, activation='linear')(x)
    model = Model(inputs=inputs, outputs=outputs)

    return model


def check_convergence(history, max_loss=None, max_val_loss=None):
    """
    Check wehether the model converged to an expected solution.

    Raises an error if train or val loss at last epoch exceeds the max values specified.
    """
    # Check train loss.
    if max_loss is not None:
        loss = history.history['loss'][-1]
        if loss > max_loss:
            raise AssertionError('Model did not converge: loss={:.3g}, max_loss={}.'
                                 .format(loss, max_loss))

    # Check validation loss.
    if max_val_loss is not None:
        val_loss = history.history['val_loss'][-1]
        if val_loss > max_val_loss:
            raise AssertionError('Model did not converge: val_loss={:.3g}, max_val_loss={}.'
                                 .format(val_loss, max_val_loss))


def plot_history(history, ax=None):
    """
    Plot train and validation loss from history object as returned by model.fit().
    """
    if ax is None:
        # Plot in current axes.
        ax = plt.gca()

    train_loss = history.history['loss']
    val_loss = history.history['val_loss']
    epochs = history.epoch

    ax.plot(epochs, train_loss, label='Train')
    ax.plot(epochs, val_loss, label='Val')

    ax.set_xlabel('Epoch (-)')
    ax.set_ylabel('Loss (a.u.)')
    ax.legend()
    ax.set_title('Training history')


def mean_var_loss(y_true, y_pred):
    """
    Mean-variance loss (Eq. 5, 8).

    References:
        A. Kendall and Y. Gal,
        “What Uncertainties Do We Need in Bayesian Deep Learning for Computer Vision?,”
         2017-03-15.

    Args:
        y_true: the true values for y.
        y_pred: the first axis of the last dimension should contain the mean/point estimate.
            The second axis of the last dimension corresponds to the log variance (see Eq. 8).
    """
    mean = y_pred[:, 0:1]
    log_var = y_pred[:, 1:2]
    squared_difference = 0.5*K.exp(-log_var) * K.square(y_true - mean)
    var_penalty = 0.5*log_var
    total_loss = tf.reduce_mean(squared_difference + var_penalty, axis=-1)
    return total_loss


def quantile_loss(qs):
    """
    Return a quantile (pinball) loss function.

    References:
        Koenker, R., Hallock, K.F.: Quantile regression.
        Journal of Economic Perspectives 15(4), 143-156 (2001). DOI 10.1257/jep.15.4.143

        https://www.kaggle.com/ulrich07/quantile-regression-with-keras/notebook

    Args:
        qs (list): target quantiles (values between 0 and 1).

    Returns:
        loss (function): function that takes in y_true and y_pred, computing the pinball loss.
    """
    # To tensorflow array of constants.
    qs = tf.constant(np.array([qs]), dtype=tf.float32)

    # Define loss function for keras.
    def loss(y_true, y_pred):
        """
        Args:
            y_true: the true values for y. Shape (N, ) or (N, 1).
            y_pred: model predictions with shape (N, len(qs)), where the the axes in the last dimension
                correspond to the target quantiles. I.e., y_pred[:, i] --> qs[i].
        """
        e = y_true - y_pred
        v = tf.maximum(qs * e, (qs - 1) * e)
        return K.mean(v, axis=-1)

    return loss


def tilted_loss(q,y,f):
    e = (y-f)
    return K.mean(K.maximum(q*e, (q-1)*e), axis=-1)


class MCDropout(keras.layers.Dropout):
    """
    Monte-Carlo dropout layer.

    Always operates in training mode, i.e. applies dropout even when making predictions/at inference time.

    References:
        https://towardsdatascience.com/monte-carlo-dropout-7fd52f8b6571
    """
    def call(self, inputs, training=None):
        return super().call(inputs, training=True)
