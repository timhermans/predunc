import matplotlib.pyplot as plt
import numpy as np


def plot_regression_results(data, pred):
    """
    Creates some plots showing the results of predeicting uncertainty in regression.

    Args:
        data (dict): dict with input data and true output (labels).
            Each item correspond to a datafold (e.g. Train, Val, ...). The number of items is variable.
            The value of each item should contain a (x, y) tuple with the input and true output arrays,
            each having length N (the number of samples).
        pred (dict): dict with predicted output.
            Each item correspond to a datafold (e.g. Train, Val, ...). The keys should be the same as in `data`.
            The value of each item should contain a (lower, yp, upper) tuple, representing the
            lower limit (`lower`), the (mean) prediction (`yp`) and the upper limit (`upper`) of the prediction interval,
            each having length N (the number of samples).
    """
    # Figure options.
    figsize = 1.5 * np.array((8, 4))
    nrows, ncols = int(np.round(np.sqrt(len(data)))), int(np.ceil(np.sqrt(len(data))))
    sp_kwargs = dict(nrows=nrows, ncols=ncols, sharey='all',
                     figsize=figsize, tight_layout=True)

    # Plot y_true vs y_pred.
    fig, axes = plt.subplots(**sp_kwargs, sharex='none')
    axes = np.reshape([axes], -1)
    for i, (label, ax) in enumerate(zip(data.keys(), axes)):
        # Extract.
        x, y = data[label]
        lower, yp, upper = pred[label]

        # Plot.
        plot_regression(y, lower, yp, upper, ax=ax)
        ax.set_title('{}'.format(label))
        if i > 0:
            ax.set_ylabel('')

    # Plot error vs upper - lower.
    fig, axes = plt.subplots(**sp_kwargs, sharex='all')
    axes = np.reshape([axes], -1)
    for i, (label, ax) in enumerate(zip(data.keys(), axes)):
        # Extract.
        x, y = data[label]
        lower, yp, upper = pred[label]
        interval = upper - lower
        error = np.abs(y - yp)

        # Mask for the ones inside the prediction interval.
        inside_int = (lower <= y) & (y <= upper)

        # Plot. Use different colors for inside and outside prediction interval.
        ax.scatter(error[inside_int], interval[inside_int], color='C2', label='Inside')
        ax.scatter(error[~inside_int], interval[~inside_int], color='C3', label='Outside')

        # Figure make-up.
        ax.set_xlabel('Error (a.u.)')
        ax.set_ylabel('Prediction interval (a.u.)')
        ax.grid(True)
        ax.set_title('{}'.format(label))
        if i > 0:
            ax.set_ylabel('')

    # Plot x vs y.
    fig, axes = plt.subplots(**sp_kwargs, sharex='all')
    axes = np.reshape([axes], -1)
    for i, (label, ax) in enumerate(zip(data.keys(), axes)):
        # Extract.
        x, y = data[label]
        lower, yp, upper = pred[label]

        if x.shape[-1] == 1:
            # If only one predictor, use x on x-axis and y_true on x-axis.
            x_var = x[:, 0]*1
            y_var = y*1
            xlabel = 'x'
            ylabel = 'y true with\nprediction interval'
        else:
            # Else use y_true on x-axis and y_pred on y-axis.
            x_var = y*1
            y_var = yp*1
            ax.plot([np.min(y), np.max(y)], [np.min(y), np.max(y)], linestyle='--', color='C7')
            xlabel = 'y true'
            ylabel = 'y pred with\nprediction interval'

        idx_sort = np.argsort(x_var)
        x = x[idx_sort]
        x_var = x_var[idx_sort]
        y = y[idx_sort]
        y_var = y_var[idx_sort]
        lower = lower[idx_sort]
        upper = upper[idx_sort]

        # Mask for the ones inside the prediction interval.
        inside_int = (lower <= y) & (y <= upper)

        # Plot prediction intervals.
        ax.fill_between(x_var, lower, upper, alpha=0.5, color='C0')

        # Plot true points. Use different colors for inside and outside prediction interval.
        ax.scatter(x_var[inside_int], y_var[inside_int], color='C2', label='Inside')
        ax.scatter(x_var[~inside_int], y_var[~inside_int], color='C3', label='Outside')

        # Figure make-up.
        ax.grid(True)
        coverage = np.mean(inside_int)*100
        ax.set_title('{}\nC={:.1f}%'.format(label, coverage))
        if i % ncols == 0:
            ax.set_ylabel(ylabel)
        if i >= (nrows - 1)*-ncols:
            ax.set_xlabel(xlabel)


def plot_regression(y, lower, yp, upper, ax=None):
    """
    Plot results of a regression model with a prediction interval (y_true vs y_pred).
    
    Args:
        y (np.ndarray): 1D array with length N containing true y values. 
        lower (np.ndarray): 1D array with length N containing lower limit of prediction interval.
        yp (np.ndarray): 1D array with length N containing centre/mean of the prediction interval.
        upper (np.ndarray): 1D array with length N containing upper limit of prediction interval.
        ax (plt.Axes): axes in which to plot. If None, plots in current axes.
    """
    if ax is None:
        # Get current axes.
        ax = plt.gca()

    # Sort for getting nice plots.
    idx_sort = np.argsort(upper-lower)
    y = y[idx_sort]
    lower = lower[idx_sort]
    yp = yp[idx_sort]
    upper = upper[idx_sort]

    # Mask for the ones inside the prediction interval.
    inside_int = (lower <= y) & (y <= upper)

    # Figure make-up.
    ax.set_xlabel('ordered samples')
    ax.set_ylabel('y true and prediction interval')
    ax.grid(True)

    # Scatter x vs mean predictions. Use different colors for inside and outside prediction interval.
    # x = y
    x = np.arange(len(y))
    ax.scatter(x[inside_int], y[inside_int], color='C2', label='Inside')
    ax.scatter(x[~inside_int], y[~inside_int], color='C3', label='Outside')

    # Indicate confidence interval by filling the area between low and high limits.
    ax.fill_between(x, lower, upper, alpha=0.5, zorder=-1, color='C0')
