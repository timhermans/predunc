import time
from datetime import timedelta


class Timer(object):
    """
    Implements a context-manager timer.

    Args:
        ndecimals (int): number of decimals to print (for the seconds).
            By default rounds to 0 decimals, i.e., whole seconds.

    Examples:
        >>> with Timer():  # doctest: +SKIP
        ...     s = [x**2 for x in range(10000000)]
        Elapsed time: 0:00:02.172509

    """
    def __init__(self, message="", ndecimals=None):
        self.ndecimals = ndecimals
        self.message = message

    def __enter__(self):
        print('{}...'.format(self.message))
        self.start = time.time()

    def __exit__(self, type, value, traceback):
        self.end = time.time()
        dt = self.end - self.start
        if self.ndecimals is not None:
            dt = round(dt, self.ndecimals)
        print("Elapsed time: {}".format(timedelta(seconds=dt)))
