import os.path
import pickle

import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

from predunc.settings import SEED, NOISE_TYPE, DATASET


def add_noise(y, noise_type, seed=SEED):
    """
    Add Gaussian noise to 1D array, with mean 0 and scale (SD) 0.05*range.

    Args:
        y (np.ndarray): 1D array to which to add noise.
        noise_type (str): select which type of noise to add. Choose from:
            'none': no noise added
            'homo': homoscedastic noise added (uniform noise).
            'hetero': heteroscedastic noise added (increasing noise with increasing y).

    Returns:
        y_out (np.ndarray): y with noise.
    """
    np.random.seed(seed)

    # Range.
    y_min = np.nanmin(y)
    y_max = np.nanmax(y)

    # Gaussian noise.
    scale = (y_max - y_min) / 10 / 2

    # Determine the weight of the noise.
    noise_type = noise_type.lower()
    if noise_type == 'none':
        # No noise.
        noise = 0
    elif noise_type == 'homo':
        # Gaussian noise, uniform weight (homoscedastic).
        noise_weight = 1
        noise = noise_weight * np.random.normal(loc=0, scale=scale, size=y.shape)
    elif noise_type == 'homo_uni':
        # Uniform noise, uniform weight (homoscedastic).
        noise_weight = 1
        noise = noise_weight * (np.random.rand(*y.shape) - 0.5)*scale*2
    elif noise_type == 'hetero':
        # Gaussian noise, noise correlates with y (heteroscedastic).
        noise_weight = ((y - y_min) / (y_max - y_min))**2 * 6
        noise = noise_weight * np.random.normal(loc=0, scale=scale, size=y.shape)
    elif noise_type == 'hetero_uni':
        # Uniform noise, noise correlates with y (heteroscedastic).
        noise_weight = ((y - y_min) / (y_max - y_min))**2 * 6
        noise = noise_weight * (np.random.rand(*y.shape) - 0.5)*scale
    else:
        raise ValueError('Invalid choice for noise_type="{}".'.format(noise_type))

    # Add noise.
    y_out = y + noise

    return y_out


def get_data(dataset=DATASET, val_split=0.15, test_split=0.15, ood_split=0.05,
             seed=SEED, normalize=True, noise_type=NOISE_TYPE):
    """
    Get data for regression problem.

    Args:
        dataset (str): which data to load. See code for options.
        val_split (float): value between 0 and 1 indicating the relative size of the validation set.
        test_split (float): value between 0 and 1 indicating the relative size of the test set.
        ood_split (float): value between 0 and 1 indicating the relative size of the out-of-data set.
        seed (int): seed for random generators.
        normalize (bool): whether to normalize the data (x and y).
        noise_type (str): select which type of noise to add. Choose from:
            'none': no noise added
            'homo': homoscedastic noise added (uniform noise).
            'hetero': heteroscedastic noise added (increasing noise with increasing quality).

    Returns:
        data (dict): dict where each item refers to a different datafold.
            Each item consists of (x, y) tuples.
    """
    dataset = dataset.lower()
    if dataset == 'boston':
        # Load all data.
        x, y = tf.keras.datasets.boston_housing.load_data(test_split=0, seed=seed)[0]
    elif dataset == 'wine':
        x, y = load_wine_data()
    elif dataset == 'sim':
        x, y = simulate_data()
    else:
        raise ValueError('Invalid options dataset="{}".'.format(dataset))

    # Add noise to y (if noise_type=='none', no noise is added).
    y = add_noise(y, noise_type)

    # Split OOD samples.
    if x.shape[-1] == 1:
        # If ony 1 predictor, make the split based on x.
        split_var = x[:, 0]
    else:
        # Otherwise, split based on y values.
        split_var = y

    # Take the ood_split/2*100% smallest and largest samples.
    qlow, qhigh = np.nanpercentile(split_var, 100*np.array([ood_split/2, 1-ood_split/2]))
    mask_ood = (split_var < qlow) | (split_var >= qhigh)
    x_ood = x[mask_ood]
    y_ood = y[mask_ood]
    x = x[~mask_ood]
    y = y[~mask_ood]

    # Update remaining split probabilities.
    test_split /= (1 - ood_split)
    val_split /= (1 - ood_split)

    # Split test set.
    x, x_test, y, y_test = train_test_split(x, y, test_size=test_split, random_state=seed)

    # Update remaining split probabilities.
    val_split /= (1 - test_split)

    # Split val set.
    x_train, x_val, y_train, y_val = train_test_split(x, y, test_size=val_split, random_state=seed)

    # Normalize.
    if normalize:
        x_normalizer = StandardScaler()
        x_train = x_normalizer.fit_transform(x_train)
        x_val = x_normalizer.transform(x_val)
        x_test = x_normalizer.transform(x_test)
        x_ood = x_normalizer.transform(x_ood)
        y_normalizer = StandardScaler()
        y_train = y_normalizer.fit_transform(y_train.reshape(-1, 1))[:, 0]
        y_val = y_normalizer.transform(y_val.reshape(-1, 1))[:, 0]
        y_test = y_normalizer.transform(y_test.reshape(-1, 1))[:, 0]
        y_ood = y_normalizer.transform(y_ood.reshape(-1, 1))[:, 0]

    data = dict(
        Train=(x_train, y_train),
        Val=(x_val, y_val),
        Test=(x_test, y_test),
        OOD=(x_ood, y_ood),
    )

    return data


def load_wine_data():
    """
    Load wine data from UCI database.

    If the code fails to load the data automatically, try downloading the data manually from
    https://archive.ics.uci.edu/ml/datasets/Wine+Quality.')

    Returns:
        x, y: input, output arrays.
    """
    cache_filepath = 'temp_wine_data.pkl'

    if os.path.exists(cache_filepath):
        # Load from cache.
        with open(cache_filepath, "rb") as f:
            data = pickle.load(f)
        x = data['x']
        y = data['y']
    else:
        # Link to online database.
        url = r'https://archive.ics.uci.edu/ml/machine-learning-databases/wine-quality/winequality-{}.csv'

        # Use pandas to read the csv files.
        df = pd.DataFrame()
        for which in ['red', 'white']:
            fp = url.format(which)
            df_i = pd.read_csv(fp, delimiter=';')
            df = pd.concat([df, df_i], ignore_index=True)

        # Separate x and y.
        x = df.drop(columns=['quality']).values
        y = df['quality'].values

        # Save to cache file.
        data = {'x': x, 'y': y}
        with open(cache_filepath, "wb") as f:
            pickle.dump(data, f)

    return x, y


def simulate_data(num=1000, seed=SEED):
    """
    Simulate y = x + sin(pi/2 * x).

    References:
        https://www.evergreeninnovations.co/blog-quantile-loss-function-for-machine-learning/

    Args:
        num (int): number of examples to create.

    Returns:
        x, y (tuple): input and output values (length num).
    """
    start = 0
    end = 4
    np.random.rand(seed)
    x = np.sort(np.random.rand(num) * (end - start) + start)
    y = x + np.sin(np.pi * x / 2)
    return x.reshape(-1, 1), y
