import numpy as np
import pandas as pd
from sklearn.metrics import mean_squared_error


def evaluate_regression(data, pred):
    """
    Compute evaluation metrics.

    Args:
        data (dict): dict with input data and true output (labels).
            Each item correspond to a datafold (e.g. Train, Val, ...). The number of items is variable.
            The value of each item should contain a (x, y) tuple with the input and true output arrays,
            each having length N (the number of samples).
        pred (dict): dict with predicted output.
            Each item correspond to a datafold (e.g. Train, Val, ...). The keys should be the same as in `data`.
            The value of each item should contain a (lower, yp, upper) tuple, representing the
            lower limit (`lower`), the (mean) prediction (`yp`) and the upper limit (`upper`) of the prediction interval,
            each having length N (the number of samples).

    Returns:
        metrics (pd.DataFrame): pandas DataFrame with metrics for each datafold.
    """
    df = pd.DataFrame()
    for label in data.keys():
        # Extract.
        x, y = data[label]
        lower, yp, upper = pred[label]
        interval = upper - lower

        # Compute metrics.
        metrics = {
            'MSE': mean_squared_error(y, yp),
            'Coverage': 100*np.mean((lower <= y) & (y <= upper)),
            'Mean interval': np.mean(interval),
        }

        # Collect in dataframe.
        df_i = pd.DataFrame(metrics, index=[label])
        df = pd.concat([df, df_i])

    return df
