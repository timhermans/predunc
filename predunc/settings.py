"""
Default settings used in the scripts.
"""
import tensorflow as tf
import scipy.stats

# Random seed.
SEED = 43

# Default dataset to use. See data.get_data().
DATASET = 'sim'

# Test and validation split sizes.
TEST_SPLIT = 0.15
VAL_SPLIT = 0.15
OOD_SPLIT = 0.10

# Dropout probability.
P_DROPOUT = 0.1

# Number of Monte Carlo dropout passes.
NUM_MC = 50

# Significance level.
ALPHA = 0.1

# Two tailed z-score with significance level.
Z_ALPHA = scipy.stats.norm.ppf((1 - ALPHA/2))

# Lower and upper quantiles.
Q_LOWER = ALPHA / 2 * 100
Q_UPPER = (1 - ALPHA / 2) * 100

# Noise type to add to y (for regression). See data.add_noise().
NOISE_TYPE = 'hetero'


def get_settings_nn():
    """
    Returns default settings for training neural networks.

    Returns:
        settings (dict): dict with settings.
    """
    callbacks = [
        # EarlyStopping(monitor='val_loss', patience=20, restore_best_weights=True),
    ]

    # Kwargs for model.compile().
    compil = dict(
        loss="mean_squared_error",
        optimizer=tf.keras.optimizers.Adam(),
    )

    # Kwargs for model.fit().
    fit = dict(
        epochs=100,
        batch_size=10,
        callbacks=callbacks,
    )

    # Collect.
    settings = dict(
        compile=compil,
        fit=fit,
    )
    return settings
