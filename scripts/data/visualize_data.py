"""
This script visualizes the data.

Author: Tim Hermans (tim.hermans@esat.kuleuven.be).
"""
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

from predunc.data import get_data
from predunc.settings import DATASET

plt.close('all')
try:
    plt.style.use('pres')
except OSError:
    pass

# Load data.
dataset = DATASET
data = get_data(dataset=dataset, normalize=False)

# Figure options.
figsize = 1.5 * np.array((8, 4))
nrows, ncols = int(np.round(np.sqrt(len(data)))), int(np.ceil(np.sqrt(len(data))))
sp_kwargs = dict(nrows=nrows, ncols=ncols, sharey='all',
                 figsize=figsize, tight_layout=True)

# Plot data.
fig, axes = plt.subplots(**sp_kwargs, sharex='all')
axes = np.reshape([axes], -1)
for (label, (x, y)), ax in zip(data.items(), axes):
    sns.histplot(y, binwidth=2, ax=ax)
    ax.set_title('{} n={}'.format(label, len(y)))
    ax.set_xlabel('y')

if dataset == 'sim':
    # Plot x vs y.
    fig, axes = plt.subplots(**sp_kwargs, sharex='all')
    axes = np.reshape([axes], -1)
    for (label, (x, y)), ax in zip(data.items(), axes):
        ax.scatter(x, y)
        ax.set_title('{} n={}'.format(label, len(y)))
        ax.set_xlabel('x')
        ax.set_ylabel('y')
