"""
This script trains a baseline regression NN.

See Also:
    https://machinelearningmastery.com/regression-tutorial-keras-deep-learning-library-python/

Author: Tim Hermans (tim.hermans@esat.kuleuven.be).
"""
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import mean_squared_error

from predunc.data import get_data
from predunc.evaluation import evaluate_regression
from predunc.keras import plot_history, build_model
from predunc.plotting import plot_regression_results
from predunc.settings import get_settings_nn, Z_ALPHA, DATASET
from predunc.utils import Timer


def baseline(dataset=DATASET, silent=False):
    # Load data.
    data = get_data(dataset=dataset)
    x_train, y_train = data['Train']
    x_val, y_val = data['Val']

    # Get settings.
    settings = get_settings_nn()

    # Build model.
    model = build_model(n_in=x_train.shape[1:], n_out=1, mc_dropout=False)

    # Compile model.
    model.compile(**settings['compile'])

    # Train model.
    history = model.fit(x_train, y_train, validation_data=(x_val, y_val),
                        **settings['fit'], verbose=2 if not silent else 0)

    if not silent:
        # Plot losses.
        plt.figure()
        plot_history(history)

    # Predict on val data.
    x, y = data['Val']
    yp = model.predict(x)

    # Compute error on val data.
    mse_val = mean_squared_error(y, yp)

    with Timer('Predicting'):
        pred = dict()
        for label, (x, y) in data.items():
            # Predict.
            yp = model.predict(x)[:, 0]

            # Determine low and high values.
            y_std = np.ones(len(yp)) * np.sqrt(mse_val)
            lower, upper = yp - Z_ALPHA * y_std, yp + Z_ALPHA * y_std

            # Save.
            pred[label] = lower, yp, upper

    # Compute metrics.
    metrics = evaluate_regression(data, pred)

    if not silent:
        # Print metrics.
        print(metrics)

        # Plot
        plot_regression_results(data, pred)

    return data, model, pred, metrics


if __name__ == '__main__':
    # Run main function.
    plt.close('all')
    try:
        plt.style.use('pres')
    except OSError:
        pass

    data, model, pred, metrics = baseline()
