"""
This script trains all neural networks and computes metrics.

Author: Tim Hermans (tim.hermans@esat.kuleuven.be).
"""
import numpy as np
import matplotlib.pyplot as plt

import pandas as pd
import seaborn as sns

from predunc.settings import ALPHA
from scripts.neural_net.baseline import baseline
from scripts.neural_net.mc_dropout import mc_dropout
from scripts.neural_net.mc_mean_var import mc_mean_var
from scripts.neural_net.mc_mean_var_conformal import mc_mean_var_conformal
from scripts.neural_net.mean_var import mean_var
from scripts.neural_net.quantile import quantile
from scripts.neural_net.quantile_conformal import quantile_conformal

plt.close('all')
try:
    plt.style.use('pres')
except OSError:
    pass

# Dict with names and functions of all networks to compare.
# Evaluating the function should return the metrics.
all_nets = {
    'Baseline': baseline,
    'Mean variance': mean_var,
    'MC dropout': mc_dropout,
    'MC mean variance': mc_mean_var,
    'Conformal MC mean variance': mc_mean_var_conformal,
    'Quantile regression': quantile,
    'Conformal quantile regression': quantile_conformal,
}

# Init dataframe with all metrics.
metrics = pd.DataFrame()

# Loop over nets and compute metrics.
for name, fun in all_nets.items():
    print('Processing {}...'.format(name))
    metrics_i = fun(silent=True)[-1].T
    metrics_i['Metric'] = metrics_i.index
    metrics_i['Method'] = name
    metrics = pd.concat([metrics, metrics_i], ignore_index=True)
    print(metrics)

#%% Print table.
metrics_table = metrics.sort_values('Metric')
index = pd.MultiIndex.from_arrays(
    [metrics_table['Metric'], metrics_table['Method']], names=["Metric", "Method"])
metrics_table = metrics_table.drop(columns=["Metric", "Method"])
metrics_table.index = index
print(metrics_table)

#%% Plot metrics.
# Unpivot.
metrics_long = pd.melt(
    metrics, id_vars=['Metric', 'Method'],
    value_vars=['Train', 'Val', 'Test', 'OOD'],
    var_name='Datafold', value_name='Score')

# The metrics to plot.
metrics_to_plot = metrics['Metric'].unique()

# Init fig.
nrows, ncols = 1, 3
figsize = 1.5 * np.array((12, 4))
fig, axes = plt.subplots(nrows, ncols, tight_layout=True, sharex='all', figsize=figsize)
axes = np.reshape([axes], -1)

# Loop over metrics and plot.
for i, (col, ax) in enumerate(zip(metrics_to_plot, axes)):
    # Plot.
    df_i = metrics_long[metrics_long['Metric'] == col]
    sns.barplot(data=df_i, y='Score', x='Datafold', hue='Method', ax=ax)
    ax.set_title(col)
    if i > 0:
        ax.get_legend().remove()
    if col == 'Coverage':
        ax.axhline((1-ALPHA)*100, color='C7', linestyle='--', zorder=-1)