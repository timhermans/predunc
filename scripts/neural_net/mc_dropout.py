"""
This script trains a neural network with Dropout with a typical loss function and
uses Monte Carlo dropout to estimate the uncertainty.

Author: Tim Hermans (tim.hermans@esat.kuleuven.be).
"""

import numpy as np
import matplotlib.pyplot as plt

from predunc.data import get_data
from predunc.evaluation import evaluate_regression
from predunc.keras import plot_history, build_model
from predunc.plotting import plot_regression_results
from predunc.settings import get_settings_nn, NUM_MC, Q_UPPER, Q_LOWER, Z_ALPHA, DATASET
from predunc.utils import Timer


def mc_dropout(dataset=DATASET, silent=False):
    # Load data.
    data = get_data(dataset=dataset)
    x_train, y_train = data['Train']
    x_val, y_val = data['Val']

    # Get settings.
    settings = get_settings_nn()

    # Build model.
    model = build_model(n_in=x_train.shape[1:], n_out=1, mc_dropout=True)

    # Compile model.
    model.compile(**settings['compile'])

    # Train model.
    history = model.fit(x_train, y_train, validation_data=(x_val, y_val),
                        **settings['fit'], verbose=2 if not silent else 0)

    if not silent:
        # Plot losses.
        plt.figure()
        plot_history(history)

    with Timer('Predicting'):
        pred = dict()
        for label, (x, y) in data.items():
            # Predict.
            y_pred = np.hstack([model.predict(x) for _ in range(NUM_MC)])

            # Determine low, mid, high values.
            # lower, yp, upper = np.percentile(y_pred, q=[Q_LOWER, 50, Q_UPPER], axis=-1)
            yp = np.mean(y_pred, axis=-1)
            y_std = np.std(y_pred, axis=-1)
            lower, upper = yp - Z_ALPHA * y_std, yp + Z_ALPHA * y_std

            # Save.
            pred[label] = lower, yp, upper

    # Compute metrics.
    metrics = evaluate_regression(data, pred)

    if not silent:
        # Print metrics.
        print(metrics)

        # Plot
        plot_regression_results(data, pred)

    return data, model, pred, metrics


if __name__ == '__main__':
    # Run main function.
    plt.close('all')
    try:
        plt.style.use('pres')
    except OSError:
        pass

    data, model, pred, metrics = mc_dropout()
