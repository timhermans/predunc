"""
This script trains a quantile regression neural net using the pinball loss.

Author: Tim Hermans (tim.hermans@esat.kuleuven.be).
"""
import os

import numpy as np
import matplotlib.pyplot as plt

import pandas as pd
import seaborn as sns

from predunc.data import get_data
from predunc.evaluation import evaluate_regression
from predunc.keras import quantile_loss, build_model, plot_history
from predunc.plotting import plot_regression_results
from predunc.settings import get_settings_nn, ALPHA, DATASET
from predunc.utils import Timer


def quantile(dataset=DATASET, silent=False):
    # Load data.
    data = get_data(dataset=dataset)
    x_train, y_train = data['Train']
    x_val, y_val = data['Val']

    # Get settings.
    settings = get_settings_nn()

    # Set the loss function for a quantile regressor.
    softening_factor = 1
    settings['compile']['loss'] = quantile_loss(qs=[softening_factor*ALPHA/2,
                                                    1 - softening_factor*ALPHA/2])

    # Build model.
    model = build_model(n_in=x_train.shape[1:], n_out=2,
                        mc_dropout=False)

    # Compile model.
    model.compile(**settings['compile'])

    # Train model.
    settings['fit']['epochs'] = 100
    history = model.fit(x_train, y_train, validation_data=(x_val, y_val),
                        **settings['fit'], verbose=2 if not silent else 0)

    if not silent:
        # Plot losses.
        plt.figure()
        plot_history(history)

    with Timer('Predicting'):
        pred = dict()
        for label, (x, y) in data.items():
            # Predict.
            y_pred = model.predict(x)
            lower, upper = [y_pred[:, i] for i in range(2)]
            yp = (lower + upper)/2

            # Save.
            pred[label] = lower, yp, upper

        # Compute metrics.
        metrics = evaluate_regression(data, pred)

        if not silent:
            # Print metrics.
            print(metrics)

            # Plot
            plot_regression_results(data, pred)

    return data, model, pred, metrics


if __name__ == '__main__':
    plt.close('all')
    try:
        plt.style.use('pres')
    except OSError:
        pass

    data, model, pred, metrics = quantile()
