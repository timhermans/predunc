"""
This script trains a mean variance estimator with MC dropout and conformal calibration.

References:
    A. Kendall and Y. Gal,
    “What Uncertainties Do We Need in Bayesian Deep Learning for Computer Vision?,”
     2017-03-15.

    Angelopoulos & Bates,
    "A Gentle Introduction to Conformal Prediction and Distribution-Free
    Uncertainty Quantification" 2022

Author: Tim Hermans (tim.hermans@esat.kuleuven.be).
"""

import numpy as np
import matplotlib.pyplot as plt

from predunc.data import get_data
from predunc.evaluation import evaluate_regression
from predunc.keras import plot_history, mean_var_loss, build_model
from predunc.plotting import plot_regression_results
from predunc.settings import get_settings_nn, NUM_MC, Z_ALPHA, DATASET, ALPHA
from predunc.utils import Timer


def mc_mean_var_conformal(dataset=DATASET, silent=False):
    # Load data.
    data = get_data(dataset=dataset)
    x_train, y_train = data['Train']
    x_val, y_val = data['Val']

    # Get settings.
    settings = get_settings_nn()

    # Set the loss function for a mean-variance estimator.
    settings['compile']['loss'] = mean_var_loss

    # Build model.
    model = build_model(n_in=x_train.shape[1:], n_out=2, mc_dropout=True)

    # Compile model.
    model.compile(**settings['compile'])

    # Train model.
    history = model.fit(x_train, y_train, validation_data=(x_val, y_val),
                        **settings['fit'], verbose=2 if not silent else 0)

    if not silent:
        # Plot losses.
        plt.figure()
        plot_history(history)

    # Predict on val data.
    x, y = data['Val']

    # Predict.
    y_pred = np.dstack([model.predict(x) for _ in range(NUM_MC)])
    yp = np.mean(y_pred[:, 0], axis=-1)

    # Determine total variance and std (Eq. 9). Note that y_pred[:, 1] is the log variance.
    y_var = np.var(y_pred[:, 0], axis=-1) + np.mean(np.exp(y_pred[:, 1]), axis=-1)
    y_std = np.sqrt(y_var)

    # Compute noncomformity scores.
    scores = np.abs(y - yp)/y_std

    # Compute critical quantile.
    n = len(y)
    q_crit = np.nanpercentile(scores, np.ceil((1 - ALPHA)*(n + 1))/n*100)

    with Timer('Predicting'):
        pred = dict()
        for label, (x, y) in data.items():
            # Predict.
            y_pred = np.dstack([model.predict(x) for _ in range(NUM_MC)])
            yp = np.mean(y_pred[:, 0], axis=-1)

            # Determine total variance and std (Eq. 9). Note that y_pred[:, 1] is the log variance.
            y_var = np.var(y_pred[:, 0], axis=-1) + np.mean(np.exp(y_pred[:, 1]), axis=-1)
            y_std = np.sqrt(y_var)

            # Determine low, mid, high values.
            lower, upper = yp - q_crit * y_std, yp + q_crit * y_std

            # Save.
            pred[label] = lower, yp, upper

    # Compute metrics.
    metrics = evaluate_regression(data, pred)

    if not silent:
        # Print metrics.
        print(metrics)

        # Plot
        plot_regression_results(data, pred)

    return data, model, pred, metrics


if __name__ == '__main__':
    # Run main function.
    plt.close('all')
    try:
        plt.style.use('pres')
    except OSError:
        pass

    data, model, pred, metrics = mc_mean_var_conformal()
