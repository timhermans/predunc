"""
This script trains a mean variance estimator (without dropout).

References:
    A. Kendall and Y. Gal,
    “What Uncertainties Do We Need in Bayesian Deep Learning for Computer Vision?,”
     2017-03-15.

Author: Tim Hermans (tim.hermans@esat.kuleuven.be).
"""
import numpy as np
import matplotlib.pyplot as plt

from predunc.data import get_data
from predunc.evaluation import evaluate_regression
from predunc.keras import plot_history, mean_var_loss, build_model
from predunc.plotting import plot_regression_results
from predunc.settings import get_settings_nn, Z_ALPHA, DATASET
from predunc.utils import Timer


def mean_var(dataset=DATASET, silent=False):
    # Load data.
    data = get_data(dataset=dataset)
    x_train, y_train = data['Train']
    x_val, y_val = data['Val']

    # Get settings.
    settings = get_settings_nn()

    # Set the loss function for a mean-variance estimator.
    settings['compile']['loss'] = mean_var_loss

    # Build model.
    model = build_model(n_in=x_train.shape[1:], n_out=2, mc_dropout=False)

    # Compile model.
    model.compile(**settings['compile'])

    # Train model.
    history = model.fit(x_train, y_train, validation_data=(x_val, y_val),
                        **settings['fit'], verbose=2 if not silent else 0)

    if not silent:
        # Plot losses.
        plt.figure()
        plot_history(history)

    with Timer('Predicting'):
        pred = dict()
        for label, (x, y) in data.items():
            # Predict.
            y_pred = model.predict(x)

            # Determine low, mid, high values.
            yp = y_pred[:, 0]
            y_std = np.sqrt(np.exp(y_pred[:, 1]))
            lower, upper = yp - Z_ALPHA * y_std, yp + Z_ALPHA * y_std

            # Save.
            pred[label] = lower, yp, upper

    # Compute metrics.
    metrics = evaluate_regression(data, pred)

    if not silent:
        # Print metrics.
        print(metrics)

        # Plot
        plot_regression_results(data, pred)

    return data, model, pred, metrics


if __name__ == '__main__':
    # Run main function.
    plt.close('all')
    try:
        plt.style.use('pres')
    except OSError:
        pass

    data, model, pred, metrics = mean_var()
