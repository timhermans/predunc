# Predicting uncertainty
Code accompanying the attached presentation about uncertainty prediction for deep learning regression.

## References
* Nicolas Dewolf, Bernard De Baets, Willem Waegeman, Valid prediction intervals for regression problems, 2021
* Kendall & Gal, What Uncertainties Do We Need in Bayesian Deep Learning for Computer Vision? 2017
* Angelopoulos & Bates, A Gentle Introduction to Conformal Prediction and Distribution-Free Uncertainty Quantification, 2022
* Romano et al. Conformalized Quantile Regression, 2019

## Contact
Tim Hermans (tim.hermans@esat.kuleuven.be)